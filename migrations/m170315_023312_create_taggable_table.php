<?php

use yii\db\Migration;
use yii\db\mysql\Schema;
/**
 * Handles the creation of table `taggable`.
 */
class m170315_023312_create_taggable_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%taggable}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'frequency' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
        ]);

        $this->createTable('{{%post_tag_assn}}', [
            'post_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'tag_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);

        $this->addPrimaryKey('', '{{%post_tag_assn}}', ['post_id', 'tag_id']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%taggable}}');
        $this->dropTable('{{%post_tag_assn}}');
    }
}
