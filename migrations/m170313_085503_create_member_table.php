<?php

use yii\db\Migration;

/**
 * Handles the creation of table `member`.
 */
class m170313_085503_create_member_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%member}}', [
            'id' => $this->primaryKey()->comment('序号'),
            'user_name' => $this->string(20)->notNull()->unique()->comment('用户名'),
            'user_email' => $this->string(200)->notNull()->comment('Email'),
            'user_password' => $this->string(20)->notNull()->comment('密码'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%member}}');
    }
}
