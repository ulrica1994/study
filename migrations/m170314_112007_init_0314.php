<?php
use yii\db\Migration;

class m170314_112007_init_0314 extends Migration
{
    public function safeUp()
    {
        $sql = file_get_contents('data/sql/init0314.sql');
        \Yii::$app->db->createCommand($sql)
            ->execute();

    }

    public function safeDown()
    {

        $this->dropTable('tbl_article');
        $this->dropTable('tbl_category');
        $this->dropTable('tbl_tag_has_article');
        $this->dropTable('tbl_comment');
        $this->dropTable('tbl_tag');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
