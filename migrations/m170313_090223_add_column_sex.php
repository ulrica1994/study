<?php

use yii\db\Migration;

class m170313_090223_add_column_sex extends Migration
{
    public function up()
    {
        $this->addColumn('{{%member}}','sex',$this->integer(2)->comment('性别'));
    }

    public function down()
    {
        $this->dropColumn('{{%member}}','sex');

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
