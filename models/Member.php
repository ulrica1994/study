<?php
/**
 * Created by PhpStorm.
 * User: fujianguo
 * Date: 2017/3/13
 * Time: 上午10:35
 */

namespace app\models;


use yii\base\Model;

/**
 * Class Member
 * @package app\models
 *
 * @property string $userName
 * @property string $password
 * @property string $confirmPassword
 * @property string $email
 *
 */
class Member extends Model
{

    public $address;

    protected $users = [
        'admin' => [
            'userName' => 'admin',
            'password' => '123456',
            'email' => 'admin@163.com',
        ],
        'demo' => [
            'userName' => 'demo',
            'password' => '654321',
            'email' => 'demo@163.com',
        ],
    ];
    protected $userName;
    protected $password;
    protected $email;
    protected $confirmPassword;

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     */
    public function setUserName($userName)
    {

        $this->userName = $userName;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getConfirmPassword()
    {
        return $this->confirmPassword;
    }

    /**
     * @param mixed $confirmPassword
     */
    public function setConfirmPassword($confirmPassword)
    {
        $this->confirmPassword = $confirmPassword;
    }


    public function rules()
    {
        return [
          [['userName','password','confirmPassword','email'],'required','message'=>'{attribute}必填'],
          [['userName'],'string','min'=>3,'max'=>20],
          [['password','confirmPassword'],'string','min'=>6,'max'=>20],
          [['email'],'string','max'=>200],
          [['email'],'email'],
          [['confirmPassword'],'compare','compareAttribute'=>'password'],
          [['password'],'verifyPassword','on'=>'login'],
            [['address'],'safe'],
        ];
    }

    public function verifyPassword($attribute)
    {
        if (!key_exists($this->userName,$this->users) || $this->users[$this->userName]['password'] != $this->password) {
            $this->addError($attribute,"用户名或密码不正确");
        }
    }

    public function register()
    {
        return $this->validate()?true:false;
    }

    public function login()
    {
        return $this->validate()?true:false;
    }

    public function attributeHints()
    {
        return [
          'password' => '密码必须大于6个字符，小于20字符',
        ];
    }

    public function attributeLabels()
    {
        return [
            'userName' => '用户名',
          'password' => '密码',
        ];
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(),[
            'login' => ['userName','password','address'],
        ]);
    }

    public function fields()
    {
        return [
          'userName','password','email','address'
        ];
    }



}