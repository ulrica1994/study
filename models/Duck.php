<?php
/**
 * Created by PhpStorm.
 * User: fujianguo
 * Date: 2017/3/10
 * Time: 上午10:51
 */

namespace app\models;


use yii\base\Component;
use yii\base\Event;

class Duck extends Component implements SwimmingInterface
{
    const EVENT_BEFORE_SWIMMING = 'beforeSwimming';
    const EVENT_AFTER_SWIMMING = 'afterSwimming';

    public $result = '';

    /**
     * @return mixed
     */
    public function swimming()
    {

        $this->trigger(self::EVENT_BEFORE_SWIMMING,new Event());
        $this->result .=  "我正在游泳!!\n";
        $this->trigger(self::EVENT_AFTER_SWIMMING,new Event());
        return $this->result;
    }




}