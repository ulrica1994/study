<?php
/**
 * Created by PhpStorm.
 * User: fujianguo
 * Date: 2017/3/15
 * Time: 上午10:52
 */

namespace app\models\queries;


use creocoder\taggable\TaggableQueryBehavior;
use yii\db\ActiveQuery;

class ArticleQuery extends ActiveQuery
{
    public function behaviors()
    {
        return [
          TaggableQueryBehavior::className(),
        ];
    }
}