<?php
/**
 * Created by PhpStorm.
 * User: fujianguo
 * Date: 2017/3/10
 * Time: 上午11:58
 */

namespace app\models;


use yii\base\Behavior;

class DuckFlyBehavior extends Behavior
{
    public function events()
    {
        return [
            Duck::EVENT_BEFORE_SWIMMING => 'beforeSwimming',
            Duck::EVENT_AFTER_SWIMMING => 'afterSwimming',
        ];
    }

    public function beforeSwimming($event)
    {
        echo 'aaaa';
        $this->owner->result = "111我准备游泳啦  ". $this->owner->result;
        $event->handled = true;
    }

    public function afterSwimming($event)
    {
        echo 'bbbbb';
        $event->sender->result .= " 上岸休息一下222";
        $event->handled = true;
    }
    public function fly()
    {
        echo 'hello';
        return 'I am flying';
    }

}