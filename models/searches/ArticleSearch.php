<?php

namespace app\models\searches;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\db\Article;

/**
 * ArticleSearch represents the model behind the search form about `app\models\db\Article`.
 */
class ArticleSearch extends Article
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'uid', 'entityId', 'categoryId', 'addTime', 'upTime', 'commentNum', 'viewNum', 'status', 'isTop'], 'integer'],
            [['title', 'face', 'content', 'keyWord'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Article::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'uid' => $this->uid,
            'entityId' => $this->entityId,
            'categoryId' => $this->categoryId,
            'addTime' => $this->addTime,
            'upTime' => $this->upTime,
            'commentNum' => $this->commentNum,
            'viewNum' => $this->viewNum,
            'status' => $this->status,
            'isTop' => $this->isTop,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'face', $this->face])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'keyWord', $this->keyWord]);

        return $dataProvider;
    }
}
