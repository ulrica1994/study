<?php
/**
 * Created by PhpStorm.
 * User: fujianguo
 * Date: 2017/3/10
 * Time: 上午11:58
 */

namespace app\models;


use yii\base\Behavior;

class DuckBehavior extends Behavior
{
    public function events()
    {
        return [
            Duck::EVENT_BEFORE_SWIMMING => 'beforeSwimming',
            Duck::EVENT_AFTER_SWIMMING => 'afterSwimming',
        ];
    }

    public function beforeSwimming($event)
    {
        echo '11111111';
        $this->owner->result = "我准备游泳啦  ". $this->owner->result;
    }

    public function afterSwimming($event)
    {
        echo '2222222';
        $event->sender->result .= " 上岸休息一下";
    }

}