<?php
/**
 * Created by PhpStorm.
 * User: fujianguo
 * Date: 2017/3/10
 * Time: 上午11:57
 */

namespace app\models;


class DuckBe extends Duck
{
    use DuckShout;
    /**
     * @return array
     */
    public function behaviors()
    {
        return [



            'fly' => [
                'class' => FlyBehavior::className(),
                'name' => '野鸭子',
            ],

//            [
//                'class' => DuckFlyBehavior::className(),
//            ],
            [
                'class'=>DuckBehavior::className(),

            ],
        ];
    }


}