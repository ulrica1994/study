<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "{{%member}}".
 *
 * @property integer $id
 * @property string $user_name
 * @property string $user_email
 * @property string $user_password
 * @property integer $sex
 */
class Member extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_name', 'user_email', 'user_password'], 'required'],
            [['sex'], 'integer'],
            [['sex'],'default','value' => 0],
            [['user_name', 'user_password'], 'string', 'max' => 20],
            [['user_email'], 'string', 'max' => 200],
            [['user_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '序号',
            'user_name' => '用户名',
            'user_email' => 'Email',
            'user_password' => '密码',
            'sex' => '性别',
        ];
    }
}
