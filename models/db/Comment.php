<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "{{%comment}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property integer $addTime
 * @property integer $tbl_rate
 * @property integer $userId
 * @property integer $commentableEntityId
 * @property integer $entityId
 * @property integer $referId
 * @property integer $voteUpNum
 * @property integer $voteDownNum
 * @property integer $deleted
 * @property integer $deleteTime
 */
class Comment extends \yii\db\ActiveRecord
{
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%comment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content', 'userId'], 'required'],
            [['content'], 'string'],
            [['addTime', 'tbl_rate', 'userId', 'commentableEntityId', 'entityId', 'referId', 'voteUpNum', 'voteDownNum', 'deleted', 'deleteTime'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '评论id',
            'title' => '标题',
            'content' => '内容',
            'addTime' => '发表时间',
            'tbl_rate' => '评分',
            'userId' => '发表者id',
            'commentableEntityId' => '评论对象id',
            'entityId' => 'Entity对象Id',
            'referId' => '引用回复',
            'voteUpNum' => '点赞数',
            'voteDownNum' => '反对数',
            'deleted' => '是否删除',
            'deleteTime' => '删除时间',
        ];
    }
}
