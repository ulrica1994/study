<?php
/**
 * Created by PhpStorm.
 * User: fujianguo
 * Date: 2017/3/10
 * Time: 下午12:08
 */

namespace app\models;


use yii\base\Behavior;

class FlyBehavior extends Behavior
{
    public $name = '';
    public $age = 7;
    public function fly()
    {
        echo 'ooo';
        return '我起飞了,'.$this->name;
    }
}