<?php
/**
 * Created by PhpStorm.
 * User: fujianguo
 * Date: 2017/3/9
 * Time: 下午3:04
 */

namespace app\models;


use yii\base\Object;

/**
 * Class Student
 * @package app\models
 *
 * @property string $name
 * @property integer $age
 * @property string $className
 *
 */
class Student extends Object
{

    public $firstName;
    public $lastName;
    public $birthday;
    public $grade;
    public $class;

    protected $_name;
    protected $_age;
    protected $_className;

    /**
     * @return mixed
     */
    public function getName()
    {
        $this->_name = $this->lastName.$this->firstName;
        return $this->_name;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        if (!$this->_age) {
            $birth = intval(date('Y',strtotime($this->birthday)));
            $today = intval(date('Y'));
            $this->_age = $today - $birth;

        }
//        var_dump($this->_age);die;
        return $this->_age;
    }


    /**
     * @return mixed
     */
    public function getClassName()
    {
        if(!$this->_className) {
            $this->_className = $this->grade.$this->class;
        }
        return $this->_className;
    }



}