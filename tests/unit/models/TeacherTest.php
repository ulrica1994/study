<?php
namespace models;


use hayiz\models\Teacher;

class TeacherTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testMe()
    {
        $teacher = new Teacher;
        expect($teacher->say())->equals('hello');
    }
}