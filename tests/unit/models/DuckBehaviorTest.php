<?php
namespace models;


use app\models\DuckBe;

class DuckBehaviorTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testMe()
    {
        $duck = new DuckBe();
        $result = $duck->swimming();

        $this->assertInternalType('string',$result);
        $this->assertStringStartsWith('我准备游泳啦',$result);
        expect($result)->endsWith('上岸休息一下');
    }

    public function testFly()
    {
        $duck = new DuckBe();
        expect($duck->fly())->startsWith('我起飞了,野鸭子');
    }

    public function testShout()
    {
        $duck = new DuckBe();
        expect($duck->shout())->equals('嘎嘎');
    }
}