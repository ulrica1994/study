<?php
namespace models;


use app\models\Duck;

class DuckTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $duck;


    protected function _before()
    {
        $duck = new Duck();
        $duck->on(Duck::EVENT_BEFORE_SWIMMING,function($event){
            $event->sender->result = "我准备游泳啦  ".$event->sender->result;
        });
        $duck->on(Duck::EVENT_AFTER_SWIMMING,function($event) {
            $event->sender->result .= " 上岸休息一下";
        });
        $this->duck = $duck;
    }

    protected function _after()
    {
    }

    // tests
    public function testMe()
    {
        $result = $this->duck->swimming();

        $this->assertInternalType('string',$result);
        $this->assertStringStartsWith('我准备游泳啦',$result);
        expect($result)->endsWith('上岸休息一下');
    }




}