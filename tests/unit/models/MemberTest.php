<?php
namespace models;


use app\models\Member;

class MemberTest extends \Codeception\Test\Unit
{
    use \Codeception\Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;

    protected $member;

    protected function _before()
    {
        $this->member = new Member();
    }

    protected function _after()
    {
    }

    // tests
    public function test属性验证()
    {
        /**@var $member Member**/
        $member = $this->member;
        //测试用户名
        verify('用户名不能为空',$member->validate(['userName']))->false();

        $member->userName = '李雷雷';
        expect_that($member->validate(['userName']));

        $member->userName = '123456789012345678901';
        verify('用户名应该介于3到20个字符',$member->validate(['userName']))->false();
        $member->userName = '12';
        verify('用户名应该介于3到20个字符',$member->validate(['userName']))->false();

        //测试密码
        verify('密码不能为空',$member->validate(['password']))->false();
        $member->password = '123456';

        expect_that($member->validate(['password']));

        $member->password = '123456789012345678901';
        verify('密码应该介于6到20个字符',$member->validate(['password']))->false();
        $member->password = '12345';
        verify('密码应该介于6到20个字符',$member->validate(['password']))->false();

        //测试确认密码
        $member->password = '123456';
        $member->confirmPassword = '1234567';
        verify('密码和确认密码应该相等',$member->validate(['confirmPassword']))->false();

        $member->confirmPassword = null;
        verify('确认密码不能为空',$member->validate(['confirmPassword']))->false();

        $member->confirmPassword = '123456';
        expect_that($member->validate(['confirmPassword']));
        $member->confirmPassword = '123456789012345678901';
        verify('确认密码应该介于6到20个字符',$member->validate(['confirmPassword']))->false();
        $member->confirmPassword = '12345';
        verify('确认密码应该介于6到20个字符',$member->validate(['confirmPassword']))->false();

        //测试Email

        verify('Email不能为空',$member->validate(['email']))->false();


        $member->email = str_pad('e',201,'a');
        expect(strlen($member->email))->equals(201);
        verify('Email应该小于200个字符',$member->validate(['email']))->false();
        $member->email = '12345';
        verify('Email应该是邮件格式',$member->validate(['email']))->false();
        $member->email = '123456@111.com';
        verify('Email应该是邮件格式',$member->validate(['email']))->true();



    }

    public function test注册()
    {
        $member = new Member();
        $member->userName = 'admin';
        $member->email = 'admin@163.com';
        $member->password = '123456';
        $member->confirmPassword = '123456';

        verify('用户注册成功',$member->register())->true();
        $member->confirmPassword = '';
        verify('用户注册失败',$member->register())->false();
    }



    public function test登录()
    {
        /**@var $member Member **/
        $member = $this->member;
        $member->setScenario('login');
        $post = [
          'Member' => [
              'userName' => 'admin',
              'password' => '123456',
              'address' => 'abcd',
          ]
        ];
        $member->load($post);

        expect($member->address)->equals('abcd');

        verify('用户登录成功',$member->login())->true();

        $member->password = '333333333';
        verify('用户登录不成功成功',$member->login())->false();


        $member = new Member();
        $member->setScenario('login');
        $data = [
            'userName' => 'admin',
            'password' => '123456',
            'address' => 'abcd',
        ];
        $member->load($data,'');

        expect($member->address)->equals('abcd');

        verify('用户登录成功',$member->login())->true();

        $member->password = '333333333';
        verify('用户登录不成功成功',$member->login())->false();


    }


}