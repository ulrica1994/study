<?php
namespace models\db;


use app\models\db\Article;
use app\models\db\Category;
use app\models\db\Comment;
use app\models\db\PostTagAssn;
use app\models\db\Tag;
use app\models\db\Taggable;

class ArticleTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function test状态描述()
    {
        $article = Article::findOne(46);
        expect($article->statusDesc)->equals('已发布');
        $article = Article::findOne(56);
        expect($article->statusDesc)->equals('未发布');
    }

    public function test热门文章()
    {
        $hots = Article::getHotsArticles();
        $this->验证文章数组($hots);


        $categoryId = 38;
        $this->验证文章数组(Article::getHotsArticles($categoryId),'viewNum',$categoryId);
    }


    public function test最新文章()
    {
        $this->验证文章数组(Article::getRecentArticles(),'addTime');
        $categoryId = 38;
        $this->验证文章数组(Article::getRecentArticles($categoryId),'addTime',$categoryId);
    }
    /**
     * @param $hots
     * @param $attribute
     * @param $categoryId
     */
    private function 验证文章数组( $hots,$attribute = 'viewNum',$categoryId = null): void
    {
        $old = null;
        foreach ($hots as $hot) {
            /**@var $hot Article */
            /**@var Article $old */
            if ($old) {
                expect(intval($hot->$attribute))->lessOrEquals(intval($old->$attribute));
                expect_that($hot->viewNum <= $old->$attribute);
                if ($hot->$attribute == $old->$attribute) {
                    expect($hot->id)->lessThan($old->id);
                }
                if ($categoryId) {
                    //文章属于同一个栏目
                    expect($hot->categoryId)->equals($old->categoryId);
                }
            }
            $old = $hot;
        }
    }

    public function test测试所属栏目()
    {
        $art = Article::findOne(50);
        $category = $art->category;
        expect_that($category instanceof Category);
        expect_that($category->id == 38);
    }

    public function test评论()
    {
        $artId = 38;
        $art = Article::findOne($artId);
        $comments = $art->comments;
        foreach ($comments as $comment) {
            expect_that($comment instanceof Comment);
            expect_that($comment->articleId == $artId);
        }
    }

    public function test关键词()
    {
        $art = Article::findOne(81);
        $tags = $art->tags;

        expect_that($tags);
        foreach ($tags as $index => $tag) {
            expect_that($tag instanceof Tag);
            expect(in_array($tag->id,[2,3]))->true();
        }
    }

    public function testAddTag()
    {
        $tag = Tag::findOne(3);
        $art = new Article(['uid'=>5]);
        expect_that($art->save());
        $art->addTag($tag);
        $tag = 2;
        $art->addTag($tag);

        $tags = $art->tags;
        expect_that(count($tags) == 2);
        foreach ($tags as $tag) {
            expect(in_array($tag->id,[2,3]))->true();
        }
        $tag = '前端';
        $art->addTag($tag);
        $I = $this->tester;
        $I->seeRecord(Tag::className(),['tag_name'=>$tag]);
    }

    public function testTaggable()
    {
        $I = $this->tester;

        $post = new Article();
        $post->uid = 5;
        // through string
        $post->tagValues = 'foo, bar, baz';

        expect_that($post->save());
        $I->seeRecord(Taggable::className(),['name'=>['foo','bar','baz']]);


        // through array
        $post->tagValues = ['foo2', 'bar2', 'baz2'];
        expect_that($post->save());
        $I->seeRecord(Taggable::className(),['name'=>'foo2']);
        $I->seeRecord(Taggable::className(),['name'=>'bar2']);
        $I->seeRecord(Taggable::className(),['name'=>'baz2']);




    }

    public function testRemoveTag()
    {
        $I = $this->tester;
        //测试删除
        $post = Article::findOne(115);
        // through string
        $post->removeTagValues('bar2');
        $post->save();
        $tag = Taggable::findOne(['name'=>'bar2']);
        $I->dontSeeRecord(PostTagAssn::className(),['tag_id'=>$tag->id,'post_id'=>$post->id]);
        $I->seeRecord(Taggable::className(),['name'=>'bar2']);
        // through array
        $post->removeTagValues(['baz2']);
        expect_that($post->save());
        $tag = Taggable::findOne(['name'=>'baz2']);
        $I->dontSeeRecord(PostTagAssn::className(),['tag_id'=>$tag->id,'post_id'=>$post->id]);
        $I->seeRecord(Taggable::className(),['name'=>'baz2']);
    }

    public function testDelete()
    {
        $I = $this->tester;

        $post = new Article();
        $post->uid = 5;
        // through string
        $post->tagValues = 'foo, bar, baz';

        expect_that($post->save());
        $I->seeRecord(Taggable::className(),['name'=>['foo','bar','baz']]);


        $post->delete();
        $tags = Taggable::findAll(['name'=>['foo','bar','baz']]);
        foreach ($tags as $tag) {
            $I->dontSeeRecord(PostTagAssn::className(),['tag_id'=>$tag->id,'post_id'=>$post->id]);
            $I->seeRecord(Taggable::className(),['name'=>$tag->name]);
        }

    }


}