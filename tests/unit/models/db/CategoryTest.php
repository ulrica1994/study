<?php
namespace models\db;


use app\models\db\Article;
use app\models\db\Category;
use yii\db\ActiveQuery;

class CategoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testArticles()
    {
        $categoryId = 38;
        $category = Category::findOne(38);
        $articles = $category->articles;
        foreach ($articles as $article) {
            /**@var Article $article **/
            verify('应该是Article对象',$article instanceof Article)->true();
            verify('栏目id应该是：'.$categoryId,$article->categoryId)->equals($categoryId);
        }
        print_r(count($articles));
        $query = $category->getArticles();
        expect_that($query instanceof ActiveQuery);
        $query->andWhere(['status' => Article::STATUS_PUBLISHED]);
        print_r($query->count()) ;

        $actives = $category->activeArticles;
        print_r(count($actives));

    }
}