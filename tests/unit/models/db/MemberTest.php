<?php
namespace models\db;

use app\models\db\Member;

class MemberTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
        Member::deleteAll();
    }

    /**
     * @param $data
     * @dataProvider data
     */
    public function test新增会员($data)
    {
        $member = new Member();

        $member->load($data,'');
        expect_that($member->save());
        expect_that(is_numeric($member->sex));

        $I = $this->tester;
        $I->seeRecord(Member::className(),['user_name'=>$member->user_name]);

    }

    /**
     * @param $data
     * @dataProvider data
     */
    public function test修改会员($data)
    {

        $I = $this->tester;
        $I->haveRecord(Member::className(),$data);
        $I->seeRecord(Member::className(),['id'=>$data['id']]);
        $member = Member::findOne($data['id']);
        verify('用户名应该等于'.$data['user_name'],$member->user_name)->equals($data['user_name']);
        $member->user_name = 'abcd';
        expect_that($member->save());
        $I->seeRecord(Member::className(),['user_name'=>'abcd']);

    }

    /**
     * @param $data
     * @dataProvider data
     */
    public function test删除会员($data)
    {

        $I = $this->tester;
        $I->haveRecord(Member::className(),$data);
        $I->seeRecord(Member::className(),['id'=>$data['id']]);
        $member = Member::findOne($data['id']);
        verify('用户名应该等于'.$data['user_name'],$member->user_name)->equals($data['user_name']);
        $member->delete();
        $I->dontSeeRecord(Member::className(),['id'=>$data['id']]);

    }

    public function data()
    {
        return [
            [
                [
                    'id' => 1,
                    'user_name' => 'admin',
                    'user_password' => '123456',
                    'user_email' => 'admin@163.com',
                    'sex' => 0,
                ],

            ],
            [
                [
                    'id' => 2,
                    'user_name' => 'demo',
                    'user_password' => '123456',
                    'user_email' => 'demo@163.com',
                    'sex' => 1,
                ]
            ]

        ];
    }


}