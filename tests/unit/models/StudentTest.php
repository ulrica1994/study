<?php
namespace models;


use app\models\Student;

class StudentTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
     * @param $data
     * @dataProvider getData
     */
    public function test姓名($data)
    {

        expect_that($student = new Student($data));
        expect_that($student->hasProperty('name'));
        expect_not($student->canSetProperty('name'));
        expect($student->name)->equals('马丽');
    }

    /**
     * @param $data
     * @dataProvider getData
     */
    public function test年龄($data)
    {
        expect_that($student = new Student($data));
        expect($student->age)->equals(7);
    }

    /**
     * @param $data
     * @dataProvider getData
     */
    public function test班级($data)
    {
        expect_that($student = new Student($data));
        expect($student->className)->equals('一年级2班');
    }

    public function getData()
    {
        return [
            'data' => [
                [
                    'firstName' => '丽',
                    'lastName' => '马',
                    'birthday' => '2010-08-10',
                    'grade' => '一年级',
                    'class' => '2班',
                ]
            ]
        ];
    }
}