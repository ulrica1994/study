<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\searches\ArticleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'uid') ?>

    <?= $form->field($model, 'entityId') ?>

    <?= $form->field($model, 'categoryId') ?>

    <?= $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'face') ?>

    <?php // echo $form->field($model, 'content') ?>

    <?php // echo $form->field($model, 'addTime') ?>

    <?php // echo $form->field($model, 'upTime') ?>

    <?php // echo $form->field($model, 'keyWord') ?>

    <?php // echo $form->field($model, 'commentNum') ?>

    <?php // echo $form->field($model, 'viewNum') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'isTop') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
